package db;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.postgres.PostgresPlugin;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;

public class DB {
    public static Jdbi postgresDB = Jdbi.create(DBCredentials.connStringPostgres).installPlugin(new PostgresPlugin())
            .installPlugin(new SqlObjectPlugin());
}
