package db;

import db.entities.Contractor;
import db.entities.mappers.ContractorMapper;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface Queries {
    @SqlQuery("SELECT inn, phone FROM nm.\"Contractors\" WHERE \"fullRegistrationDate\" IS NOT NULL AND \"blocked\" = false " +
            "AND \"overallRating\" IS NOT NULL LIMIT 50")
    @RegisterRowMapper(ContractorMapper.class)
    List<Contractor> fullRegistredContractors();
}
