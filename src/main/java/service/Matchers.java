package service;

import com.codeborne.selenide.SelenideElement;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.number.IsCloseTo;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class Matchers {
    public static Matcher<SelenideElement> isDisabled() {
        return new FeatureMatcher<SelenideElement, Boolean>(
                is(false),
                "элемент должен быть неактивен",
                "неактивность - "
        ) {
            @Override
            protected Boolean featureValueOf(SelenideElement selenideElement) {
                return selenideElement.isEnabled();
            }
        };
    }

    public static Matcher<SelenideElement> hasDoubleValue(final Double expected) {
        return new FeatureMatcher<SelenideElement, String>(
                equalTo(String.format("%.2f", expected)),
                "значение должно соответствовать ",
                "значение: "
        ) {
            @Override
            protected String featureValueOf(SelenideElement selenideElement) {
                String text = selenideElement.text().length() > 0 ? selenideElement.text() : selenideElement.val();
                text = text.replaceAll(",", ".").replaceAll("[^\\d.]", "");
                return text.length() > 0 ? String.format("%.2f", Double.parseDouble(text)) : "";
            }
        };
    }
}
