package service;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import org.testng.annotations.BeforeSuite;
import steps.*;
import steps.api.ApiAuthSteps;
import steps.api.ApiContractorStatusSteps;
import steps.api.ApiDepositSteps;

import static com.codeborne.selenide.Selenide.open;

/**
 * Links to test steps classes for using in test classes
 */
public abstract class Init {
    // UI Test Steps
    public static BaseSteps baseSteps = new BaseSteps();
    public static InfoPageSteps infoPageSteps = new InfoPageSteps();
    public static MainMenuSteps mainMenuSteps = new MainMenuSteps();
    public static LoginSteps loginSteps = new LoginSteps();
    public static OrderListSteps orderListSteps = new OrderListSteps();
    public static OrderListCalcSteps orderListCalcSteps = new OrderListCalcSteps();

    // DB/API test steps
    public static ApiAuthSteps apiAuthSteps = new ApiAuthSteps();
    public static ApiContractorStatusSteps apiContractorStatusSteps = new ApiContractorStatusSteps();
    public static ApiDepositSteps apiDepositSteps = new ApiDepositSteps();

    @BeforeSuite
    public static void testInit() {
        Configuration.browser = CustomWebDriverProvider.class.getCanonicalName();
        Configuration.baseUrl = "https://nm-test.mmtr.ru/";

        // "window-size=1920,1080" parameter doesn't work. Workaround.
        open("/login");
        WebDriverRunner.getWebDriver().manage().window().maximize();

        SelenideLogger.addListener("CustomListener", new CustomListener().screenshots(true).savePageSource(false));
    }
}

