package service;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverProvider;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selenide.open;
import static org.openqa.selenium.remote.CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION;
import static org.openqa.selenium.remote.CapabilityType.PAGE_LOAD_STRATEGY;

public class CustomWebDriverProvider implements WebDriverProvider {
    @Override
    public WebDriver createDriver(DesiredCapabilities desiredCapabilities) {
        ChromeOptions options = new ChromeOptions();

        options.addArguments("disable-logging");
        options.addArguments("disable-infobars");
        options.addArguments("disable-translate");
        options.addArguments("disable-plugins");
        options.addArguments("disable-extensions");
        options.addArguments("disable-web-security");
        options.addArguments("no-default-browser-check");
        options.addArguments("no-sandbox");
        options.addArguments("no-first-run");
        options.addArguments("silent");
        options.addArguments("no-first-run");
        options.addArguments("window-size=1920,1080");
        options.addArguments("lang=ru");
        options.addArguments("disable-gpu");
        options.addArguments("disable-notifications");

        options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, "ignore");
        options.setCapability(ENSURING_CLEAN_SESSION, true);
        options.setCapability(PAGE_LOAD_STRATEGY, "normal");

        WebDriver driver = new ChromeDriver(options);

        driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(120, TimeUnit.SECONDS);

        return driver;
    }
}