package service;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static com.codeborne.selenide.Selenide.$;

public class CommonFunctions {

    static public void jsClick(ExBy by) {
        Selenide.executeJavaScript("arguments[0].click();", $(by));
    }

    static public void jsSetValue(ExBy by, String val) {
        Selenide.executeJavaScript(String.format("arguments[0].value = '%s';", val), $(by));
    }

    static public Date currentDatePlusDays(int numberOfDays) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, numberOfDays);
        return cal.getTime();
    }

    /**
     * "55 000.2" --> "55000.20" (при fracDigits = 2)
     */
    static private String _formatNumberString(String numberString, int fracDigits) {
        numberString = numberString.replaceAll(",", ".").replaceAll("[^\\d.]", "");
        return numberString.length() > 0 ? String.format("%." + fracDigits + "f", Double.parseDouble(numberString)) : "";
    }

    static public void inputDoubleReliable(SelenideElement input, Double value, int fracDigits) {
        String expectedValue = _formatNumberString(value.toString(), 2);
        int retries = 0;

        input.clear();
        while (!_formatNumberString(input.getValue(), 2).equals(expectedValue) && retries < 20) {
            retries++;
            input.clear();
            for( char c: value.toString().toCharArray()) {
                input.sendKeys(String.valueOf(c));
                input.sendKeys(Keys.END);
            }
        }
    }

    static public void inputDoubleReliable(SelenideElement input, Double value) {
        inputDoubleReliable(input, value, 2);
    }
}
