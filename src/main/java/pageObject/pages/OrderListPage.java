package pageObject.pages;

import pageObject.elements.*;

public class OrderListPage extends BasePage {
    public OrderListFrame onOrderListFrame() {
        return new OrderListFrame();
    }

    public OrderCreationPopup onOrderCreationPopup() {
        return new OrderCreationPopup();
    }
    public DataPicker onDataPicker() {return new DataPicker();}
    public OrderCreationModalAlert onOrderCreationModalAlert() {return new OrderCreationModalAlert();}
    public OrderListTable onOrderListTable() {return new OrderListTable();}
    public OrderCreationSuccessPopup onOrderCreationSuccessPopup() {return new OrderCreationSuccessPopup();}
    public OrderListCalc onOrderListCalc() {return new OrderListCalc();}
}
