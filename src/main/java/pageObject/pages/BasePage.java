package pageObject.pages;

import pageObject.elements.MainMenuFrame;

abstract public class BasePage {

    public MainMenuFrame onMainMenuFrame() {
        return new MainMenuFrame();
    }

}
