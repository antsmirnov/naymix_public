package pageObject.pages;

import pageObject.elements.InfoFrame;

public class InfoPage extends BasePage {
    public InfoFrame onInfoFrame() {
        return new InfoFrame();
    }

}

