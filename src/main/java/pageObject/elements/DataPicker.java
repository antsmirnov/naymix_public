package pageObject.elements;

import service.ExBy;

public class DataPicker {
    public ExBy btnOpen = new ExBy.ByCssSelector(".custom-date-input__icon");
    public ExBy btnDropDownYear = new ExBy.ByCssSelector(".react-datepicker__year-read-view--down-arrow");
    public ExBy yearItems = new ExBy.ByCssSelector(".react-datepicker__year-option");
    public ExBy btnDropDownMonth = new ExBy.ByCssSelector(".react-datepicker__month-read-view--down-arrow");
    public ExBy monthItems = new ExBy.ByCssSelector(".react-datepicker__month-option");

    // Требует String.format() подстановки номера дня в формате ###, например 013
    public String currentMonthDay = ".react-datepicker__day--%s:not(.react-datepicker__day--outside-month)";
}
