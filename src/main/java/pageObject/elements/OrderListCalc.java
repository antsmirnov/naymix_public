package pageObject.elements;

import service.ExBy;

public class OrderListCalc {
    public ExBy container = new ExBy.ByCssSelector(".deposit-calc-body");
    public ExBy btnClose = new ExBy.ByCssSelector("i.close.icon");

    public ExBy radioNet = new ExBy.ByXPath("//div[contains(@class, 'radio') and ./label[text()='NET (\"на руки\" исполнителю)']]/input");
    public ExBy radioGrossWithoutCommission = new ExBy.ByXPath("//div[contains(@class, 'radio') and ./label[text()='GROSS без комиссии платформы']]/input");
    public ExBy radioGross = new ExBy.ByXPath("//div[contains(@class, 'radio') and ./label[text()='GROSS']]/input");

    public ExBy inputAmount = new ExBy.ByCssSelector("input[name=amount]");
    public ExBy inputCommission = new ExBy.ByCssSelector("input[name=clientCommission]");

    public ExBy valueDeposit = new ExBy.ByXPath("//div[@class='deposit-calc-body__field' and ./div[text()='Депозит']]/div[@class='deposit-calc-body__field-value']");
    public ExBy valueOrder = new ExBy.ByXPath("//div[@class='deposit-calc-body__field' and ./div[text()='Заказ']]/div[@class='deposit-calc-body__field-value']");
    public ExBy valueExecutor = new ExBy.ByXPath("//div[@class='deposit-calc-body__field' and ./div[text()='Исполнитель']]/div[@class='deposit-calc-body__field-value']");
    public ExBy valueTax = new ExBy.ByXPath("//div[@class='deposit-calc-body__field' and ./div[text()='Налог']]/div[@class='deposit-calc-body__field-value']");
}
