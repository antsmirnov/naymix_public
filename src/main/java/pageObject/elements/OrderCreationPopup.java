package pageObject.elements;

import service.ExBy;

public class OrderCreationPopup {

    public ExBy popupContainer = ExBy.cssSelector(".popup.order-edit");
    public ExBy btnClose = ExBy.cssSelector(".popup-header_close");

    public ExBy inputOrderCategory = ExBy.cssSelector("div[name='orderCategoryId'] > input");
    public ExBy inputOrderCategoryFirstFound = ExBy.cssSelector("div[name='orderCategoryId'] div[role='listbox'] > div:first-of-type");

    public ExBy inputOrderSubCategory = ExBy.cssSelector("div[name='orderSubcategoryId'] > input");
    public ExBy inputOrderSubCategoryFirstFound = ExBy.cssSelector("div[name='orderSubcategoryId'] div[role='listbox'] > div:first-of-type");

    public ExBy inputOrderName = ExBy.cssSelector("input[name='name']");
    public ExBy inputOrderDescription = ExBy.cssSelector("textarea[name='description']");
    public ExBy inputOrderContractorsCount = ExBy.cssSelector("input[name='contractorsNeededCount']");
    public ExBy inputOrderAmount = ExBy.cssSelector("input[name='amount']");

    public ExBy inputObject = ExBy.cssSelector("[name='objectId'] input.search");
    public ExBy inputObjectFirstFound = ExBy.cssSelector("[name='objectId'] [role='option']:first-of-type");

    public ExBy inputOrderKeywords = ExBy.cssSelector("div[name='keywords'] input");

    public ExBy btnNext = ExBy.cssSelector(".order-edit__button");

    public ExBy checkboxCheckIn = ExBy.cssSelector("*[name='checkInRequired']");
    public ExBy checkboxCheckOut = ExBy.cssSelector("*[name='checkOutRequired']");
    public ExBy checkboxCheckInPhoto = ExBy.cssSelector("*[name='checkInPhotoRequired']");
    public ExBy checkboxCheckOutPhoto = ExBy.cssSelector("*[name='checkOutPhotoRequired']");

    public ExBy textCardInfos = ExBy.cssSelector(".order-edit__card-info");

    public ExBy textOrderName = ExBy.cssSelector(".order-edit__card-app_mobile .order-edit__card-app-name");
    public ExBy textAmountPerOne = ExBy.cssSelector(".order-edit__card-app_mobile .order-edit__card-app-sum");
    public ExBy textPeriod = ExBy.cssSelector(".order-edit__card-app_mobile .order-edit__card-app-period");
    public ExBy textAddress = ExBy.cssSelector(".order-edit__card-app_mobile .order-edit__card-app-address");

    public ExBy textOrderNameFull = ExBy.cssSelector(".order-edit__card-app_full .order-edit__card-app-name");
    public ExBy textAmountPerOneFull = ExBy.cssSelector(".order-edit__card-app_full .order-edit__card-app-sum");
    public ExBy textPeriodFull = ExBy.cssSelector(".order-edit__card-app_full .order-edit__card-app-period");
    public ExBy textAddressFull = ExBy.cssSelector(".order-edit__card-app_full .order-edit__card-app-address");
    public ExBy textOrderDescriptionFull = ExBy.cssSelector(".order-edit__card-app_full .order-edit__card-app-content");

    public ExBy btnPublic = ExBy.cssSelector(".order-edit__button");

}
