package pageObject.elements;

import service.ExBy;

public class OrderListFrame {

    public ExBy btnCreateOrder = ExBy.cssSelector(".button-create-order");
    public ExBy notification = ExBy.cssSelector(".Toastify__toast-body");
    public ExBy btnOpenCalc = ExBy.cssSelector(".button-calculator.orders__controls-button");

}