package pageObject.elements;

import service.ExBy;

public class OrderCreationSuccessPopup {
    public ExBy container = new ExBy.ByCssSelector(".popup.order-create-success");
    public ExBy textHeader = new ExBy.ByCssSelector(".order-create-success__header");
    public ExBy btnClose = new ExBy.ByCssSelector("button.apply-buttons__cancel");
}
