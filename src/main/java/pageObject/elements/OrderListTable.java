package pageObject.elements;

import service.ExBy;

public class OrderListTable {
    public ExBy orderStatus = new ExBy.ByCssSelector(".order-list-status");
    public ExBy orderTitle = new ExBy.ByCssSelector(".order-list-card__title");
    public ExBy btnActions = new ExBy.ByCssSelector(".order-list-table__dropdown_green.button");
    public ExBy actionsEditOrder = new ExBy.ByXPath("//div[contains(@class, 'menu')]/div[contains(@class, 'item') and text()='Редактировать заказ']");
}
