package pageObject.elements;

import service.ExBy;

public class OrderCreationModalAlert {
    public ExBy windowContainer = ExBy.cssSelector("form.nm-confirm");
    public ExBy textMsg = ExBy.cssSelector(".nm-confirm__warning-msg");
    public ExBy btnCancel = ExBy.cssSelector("button.apply-buttons__cancel");
    public ExBy btnAccept = ExBy.cssSelector("button.apply-buttons__submit");
}
