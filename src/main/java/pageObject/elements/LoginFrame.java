package pageObject.elements;

import service.ExBy;

public class LoginFrame {

    public ExBy inputEmail = ExBy.cssSelector(".login-block input[placeholder='E-mail']").as("поле ввода E-mail");
    public ExBy inputPassword = ExBy.cssSelector(".login-block input[type='password']").as("поле ввода пароля");
    public ExBy btnLogin = ExBy.cssSelector(".button.login-block__auth_login").as("кнопка 'Вход'");

}
