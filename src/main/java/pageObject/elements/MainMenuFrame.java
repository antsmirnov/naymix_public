package pageObject.elements;

import service.ExBy;

public class MainMenuFrame {

    // Links
    public ExBy linkOrderList = ExBy.cssSelector("a[href$='order-list']").as("элемент меню 'Заказы'");

    // Username
    public ExBy divFirstName = ExBy.cssSelector(".nmx-menu__user div:nth-of-type(2)").as("элемент с именем пользователя");
    public ExBy divSecondName = ExBy.cssSelector(".nmx-menu__user div:nth-of-type(1)").as("элемент с фамилией пользователя");
    public ExBy divPatronymic = ExBy.cssSelector(".nmx-menu__user div:nth-of-type(3)").as("элемент с отчеством пользователя");

    // Logout
    public ExBy buttonLogout = ExBy.cssSelector(".nmx-menu__exit").as("кнопка выхода из системы");
}
