package steps.api;

import api.Api;
import api.model.GetInfoDepositBalanceModel;
import io.qameta.allure.Step;

import java.io.IOException;

import static service.CustomAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

public class ApiDepositSteps {

    @Step("Проверка успешного возврата результата методом depositBalance")
    public static void depositBalanceIsAccessible() {
        try {
            GetInfoDepositBalanceModel response = Api.getInfoDepositBalance.getInfoDepositBalance("Bearer " + Api.savedAccessToken).execute().body();
            assertThat("Ответ содержит поле depositAmount", response.depositAmount, not(nullValue()));
        } catch (IOException e) {
            assertThat("Запрос должен выполниться успешно", e, nullValue());
        }
    }
}
