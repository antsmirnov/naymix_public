package steps.api;

import api.Api;
import api.model.TaxCheckContractorStatusModel;
import db.DB;
import db.Queries;
import db.entities.Contractor;
import io.qameta.allure.Step;

import java.io.IOException;

import static service.CustomAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ApiContractorStatusSteps {
    @Step("Проверка ответа метода checkContractorStatus при существующих в системе телефоне и ИНН")
    public static void checkContractorStatusWithValidInnAndPhone() {
        Contractor validContractor = DB.postgresDB.withExtension(Queries.class, query -> {
            return query.fullRegistredContractors();
        }).get(0);

        try {
            TaxCheckContractorStatusModel response = Api.checkContractorStatus.checkContractorStatus(
                    "Bearer " + Api.savedAccessToken,
                    validContractor.inn,
                    validContractor.phone
            ).execute().body();
            assertThat("Ответ метода checkContractorStatus не пустой", response, notNullValue());
            assertThat("Статус должен быть = 'ОК'", response.status, comparesEqualTo("OK"));
            assertThat("Поле 'phone' должно совпадать с параметром запроса 'contractorPhone'", response.phone, equalTo(validContractor.phone));
            assertThat("Поле 'inn' должно совпадать с параметром запроса 'contractorInn'", response.inn, equalTo(validContractor.inn));
        } catch (IOException e) {
            assertThat("Запрос должен выполниться успешно", e, nullValue());
        }
    }

}
