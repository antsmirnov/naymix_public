package steps.api;

import api.Api;
import api.model.AuthLoginModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.qameta.allure.Description;
import io.qameta.allure.Step;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import testData.User;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static service.CustomAssert.assertThat;

public class ApiAuthSteps {
    @Step("Получение и сохранение токенов с помощью auth/login")
    public static void getAndSaveTokens() {

        Type mapType = new TypeToken<Map<String, String>>() {}.getType();
        String jsonCredentials = new Gson().toJson(new HashMap<String, String>() {{
            put("login", User.login);
            put("password", User.password);
        }}, mapType);

        try {
            AuthLoginModel loginResponse = Api.login
                    .login(RequestBody.create(MediaType.parse("application/json"), jsonCredentials))
                    .execute()
                    .body();
            assertThat("Ответ содержит непустое поле 'accessToken'", loginResponse.accessToken, not(emptyOrNullString()));
            assertThat("Ответ содержит непустое поле 'refreshToken'", loginResponse.refreshToken, not(emptyOrNullString()));
            Api.savedAccessToken = loginResponse.accessToken;
            Api.savedRefreshToken = loginResponse.refreshToken;
        } catch (Exception e) {
            assertThat("Запрос должен выполниться успешно", e, nullValue());
        }
    }

    @Step("Получение токенов, только в случае, если еще не были получены")
    public static void getTokensIfNeeds() {
        Type mapType = new TypeToken<Map<String, String>>() {}.getType();
        String jsonCredentials = new Gson().toJson(new HashMap<String, String>() {{
            put("login", User.login);
            put("password", User.password);
        }}, mapType);
        if (Api.savedAccessToken == null) {  // Токены еще не были получены и сохранены
            try {
                AuthLoginModel loginResponse = Api.login
                        .login(RequestBody.create(MediaType.parse("application/json"), jsonCredentials))
                        .execute()
                        .body();
                assertThat("Ответ содержит непустое поле 'accessToken'", loginResponse.accessToken, not(emptyOrNullString()));
                Api.savedAccessToken = loginResponse.accessToken;
                Api.savedRefreshToken = loginResponse.refreshToken;
            } catch (Exception e) {
                assertThat("Токены авторизации не были получены", e, nullValue());
            }
        }
    }

    @Step("Получение и сохранение токенов с помощью auth/refreshToken")
    public static void getAndSaveTokensWithRefreshToken() {
        try {
            AuthLoginModel loginResponse = Api.refreshToken.refreshToken("Bearer " + Api.savedRefreshToken).execute().body();
            assertThat("Ответ содержит непустое поле 'accessToken'", loginResponse.accessToken, not(emptyOrNullString()));
            assertThat("Ответ содержит непустое поле 'refreshToken'", loginResponse.refreshToken, not(emptyOrNullString()));
            Api.savedAccessToken = loginResponse.accessToken;
            Api.savedRefreshToken = loginResponse.refreshToken;
        } catch (IOException e) {
            assertThat("Запрос должен выполниться успешно", e, nullValue());
        }
    }
}
