package steps;

import io.qameta.allure.Step;
import pageObject.pages.InfoPage;
import pageObject.pages.LoginPage;
import pageObject.pages.OrderListPage;
import service.ExBy;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$$;

public class BaseSteps {

    @Step("Вход в систему")
    public void login(String email, String password) {
        open("login");
        $(new LoginPage().onLoginFrame().inputEmail).setValue(email);
        $(new LoginPage().onLoginFrame().inputPassword).setValue(password);
        $(new LoginPage().onLoginFrame().btnLogin).click();
        $("title").shouldHave(attribute("text", "Naimix"));
    }

    @Step("Выход из аккаунта")
    public void logout() {
        $(new InfoPage().onMainMenuFrame().buttonLogout).click();
        $(new LoginPage().onLoginFrame().btnLogin).should(exist);
    }

    @Step("Установка даты в datepickir")
    public void dataPickerSetDate(int dataPickerIndex, Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String formattedDay = ((day <= 9) ? "00" : "0") + day;

        $$(new OrderListPage().onDataPicker().btnOpen).get(dataPickerIndex).click();
        $(new OrderListPage().onDataPicker().btnDropDownYear).click();
        ($$(new OrderListPage().onDataPicker().yearItems).filterBy(text(Integer.toString(year)))).get(0).scrollIntoView(false).click();
        $(new OrderListPage().onDataPicker().btnDropDownMonth).click();
        $$(new OrderListPage().onDataPicker().monthItems).get(month).click();
        $(new ExBy.ByCssSelector(String.format(new OrderListPage().onDataPicker().currentMonthDay, formattedDay))).click();
    }
}