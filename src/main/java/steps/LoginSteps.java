package steps;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.$;

public class LoginSteps {
    @Step("Проверка заголовка на странице авторизации")
    public void checkLoginPageTitle() {
        $("title").shouldHave(attribute("text", "Наймикс - Авторизация"));
    }
}
