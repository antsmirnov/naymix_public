package steps;

import io.qameta.allure.Step;
import pageObject.pages.OrderListPage;
import service.CommonFunctions;
import testData.OrderCalc;
import testData.SalaryType;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static service.CustomAssert.assertThat;
import static service.Matchers.hasDoubleValue;

public class OrderListCalcSteps {

    @Step("Открыть калькулятор расчета пополнения депозита")
    public void openCalc() {
        $(new OrderListPage().onOrderListFrame().btnOpenCalc).click();
        $(new OrderListPage().onOrderListCalc().container).shouldBe(visible);
    }

    @Step("Выбрать тип зарплаты")
    public void setSalaryType(SalaryType type) {
        if (type == SalaryType.NET) {
            CommonFunctions.jsClick(new OrderListPage().onOrderListCalc().radioNet);
        } else if (type == SalaryType.GROSS) {
            CommonFunctions.jsClick(new OrderListPage().onOrderListCalc().radioGross);
        } else if (type == SalaryType.GROSS_WITHOUT_COMMISSION) {
            CommonFunctions.jsClick(new OrderListPage().onOrderListCalc().radioGrossWithoutCommission);
        }
    }

    // использование кастомного матчера
    @Step("Заполнить поля 'Сумма' и 'Комиссия'")
    public void setAmountAndCommission(Double amount, Double commission) {
        CommonFunctions.inputDoubleReliable($(new OrderListPage().onOrderListCalc().inputAmount), amount);
        assertThat("Сумма должна соответствовать введенной",
                $(new OrderListPage().onOrderListCalc().inputAmount), hasDoubleValue(amount));
        CommonFunctions.inputDoubleReliable($(new OrderListPage().onOrderListCalc().inputCommission), commission);
        assertThat("Сумма должна соответствовать введенной",
                $(new OrderListPage().onOrderListCalc().inputCommission), hasDoubleValue(commission));
    }

    @Step("Проверить значение в блоке 'Депозит'")
    public void checkDepositValue(OrderCalc orderCalc) {
        assertThat("Проверка значения блока Депозит",
                $(new OrderListPage().onOrderListCalc().valueDeposit), hasDoubleValue(orderCalc.getDepositValue()));
    }

    @Step("Проверить значение в блоке 'Заказ'")
    public void checkOrderValue(OrderCalc orderCalc) {
        assertThat("Проверка значения блока Заказ",
                $(new OrderListPage().onOrderListCalc().valueOrder), hasDoubleValue(orderCalc.getOrderValue()));
    }

    @Step("Проверить значение в блоке 'Исполнитель'")
    public void checkExecutorValue(OrderCalc orderCalc) {
        assertThat("Проверка значения блока Исполнитель",
                $(new OrderListPage().onOrderListCalc().valueExecutor), hasDoubleValue(orderCalc.getExecutorValue()));
    }

    @Step("Проверить значение в блоке 'Налог'")
    public void checkTaxValue(OrderCalc orderCalc) {
        assertThat("Проверка значения блока Налог",
                $(new OrderListPage().onOrderListCalc().valueTax), hasDoubleValue(orderCalc.getTaxValue()));
    }
}


