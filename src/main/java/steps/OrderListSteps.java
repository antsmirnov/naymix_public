package steps;

import api.Api;
import api.model.GetInfoDepositBalanceModel;
import io.qameta.allure.Step;
import pageObject.pages.OrderListPage;
import service.CommonFunctions;
import service.ExBy;
import steps.api.ApiAuthSteps;
import testData.Order;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.*;
import static service.CustomAssert.assertThat;

public class OrderListSteps {

    @Step("Нажать кнопку \"Создать заказ\"")
    public void openNewOrderPopup() {
        $(new OrderListPage().onOrderListFrame().btnCreateOrder).click();
    }

    /*
        На примере этого тестового шага можно видеть, что в проекте используются паттерны:
           - Value Object (использование объекта, содержащим различные параметры объекта)
           - Steps (разбиение тесткейсов на шаги, что также приближает нас к паттерну Keyword Driven Testing
                    при использовании Selenide и Allure)
           - Page Object
           - Page Elements
     */
    @Step("Заполнить первую страницу параметров нового заказа валидными данными")
    public void firstPageFilling() {
        $(new OrderListPage().onOrderCreationPopup().inputOrderCategory).sendKeys(Order.category);
        $(new OrderListPage().onOrderCreationPopup().inputOrderCategoryFirstFound).click();
        $(new OrderListPage().onOrderCreationPopup().inputOrderSubCategory).sendKeys(Order.subCategory);
        $(new OrderListPage().onOrderCreationPopup().inputOrderSubCategoryFirstFound).click();
        $(new OrderListPage().onOrderCreationPopup().inputOrderName).setValue(Order.name);
        $(new OrderListPage().onOrderCreationPopup().inputOrderDescription).setValue(Order.orderDescription);
        new BaseSteps().dataPickerSetDate(0, CommonFunctions.currentDatePlusDays(1));
        new BaseSteps().dataPickerSetDate(1, CommonFunctions.currentDatePlusDays(10));
        $(new OrderListPage().onOrderCreationPopup().inputOrderAmount).setValue(String.format("%.0f", Order.amount));
        $(new OrderListPage().onOrderCreationPopup().inputOrderContractorsCount).setValue(Order.contractorsCount.toString());
        CommonFunctions.jsClick((new OrderListPage().onOrderCreationPopup().inputObject));
        $(new OrderListPage().onOrderCreationPopup().inputObjectFirstFound).click();
    }

    @Step("Нажать кнопку \"Далее\"")
    public void newOrderClickNextButton() {
        $(new OrderListPage().onOrderCreationPopup().btnNext).click();
    }

    @Step("Проверка, активны ли чек-боксы \"Требовать отметку о прибытии/уходе\"")
    public void checkCheckBoxesCheckInOut() {
        $(new OrderListPage().onOrderCreationPopup().checkboxCheckIn).shouldBe(enabled);
        $(new OrderListPage().onOrderCreationPopup().checkboxCheckOut).shouldBe(enabled);
        Functions.checkDoubleToggleCheckBox(new OrderListPage().onOrderCreationPopup().checkboxCheckIn);
        Functions.checkDoubleToggleCheckBox(new OrderListPage().onOrderCreationPopup().checkboxCheckOut);
    }

    @Step("Проверка доступности чекбоксов (не)доступности чекбоксов 'Запрашивать фотографию'")
    public void checkCheckBoxesRequirePhoto() {
        $(new OrderListPage().onOrderCreationPopup().checkboxCheckInPhoto).shouldBe(visible).shouldNotBe(enabled);
        CommonFunctions.jsClick(new OrderListPage().onOrderCreationPopup().checkboxCheckIn);
        $(new OrderListPage().onOrderCreationPopup().checkboxCheckInPhoto).shouldBe(enabled);
        CommonFunctions.jsClick(new OrderListPage().onOrderCreationPopup().checkboxCheckIn);
        $(new OrderListPage().onOrderCreationPopup().checkboxCheckInPhoto).shouldBe(visible).shouldNotBe(enabled);
        $(new OrderListPage().onOrderCreationPopup().checkboxCheckOutPhoto).shouldBe(visible).shouldNotBe(enabled);
        CommonFunctions.jsClick(new OrderListPage().onOrderCreationPopup().checkboxCheckOut);
        $(new OrderListPage().onOrderCreationPopup().checkboxCheckOutPhoto).shouldBe(enabled);
        CommonFunctions.jsClick(new OrderListPage().onOrderCreationPopup().checkboxCheckOut);
        $(new OrderListPage().onOrderCreationPopup().checkboxCheckOutPhoto).shouldBe(visible).shouldNotBe(enabled);
    }

    @Step("Проверка содержания окна 'Публикация'")
    public void checkPublicationWindow() {
        $$(new OrderListPage().onOrderCreationPopup().textCardInfos).get(0)
                .shouldHave(text("Так ваш заказ будет отображаться в списке поиска для исполнителей\n" +
                        "В заказе отображается сумма из расчета на 1 исполнителя"));
        $(new OrderListPage().onOrderCreationPopup().textOrderName).shouldHave(text(Order.name));
        $(new OrderListPage().onOrderCreationPopup().textAmountPerOne).shouldHave(text(Functions.formatAmount(Order.amountPerOneContractor)));
        $$(new OrderListPage().onOrderCreationPopup().textCardInfos).get(1)
                .shouldHave(text("И так при просмотре подробной информации перед отправкой отклика"));
        $(new OrderListPage().onOrderCreationPopup().textOrderNameFull).shouldHave(text(Order.name));
        $(new OrderListPage().onOrderCreationPopup().textAmountPerOneFull).shouldHave(text(Functions.formatAmount(Order.amountPerOneContractor)));
        $(new OrderListPage().onOrderCreationPopup().textOrderDescriptionFull).shouldHave(text(Order.orderDescription));
    }

    @Step("Опубликовать заказ")
    public void pubicTheOrder() {
        $(new OrderListPage().onOrderCreationPopup().btnPublic).shouldBe(visible).shouldBe(enabled).click();
        $(new OrderListPage().onOrderCreationModalAlert().windowContainer).should(visible);
        $(new OrderListPage().onOrderCreationModalAlert().textMsg).shouldBe(visible)
                .shouldHave(text("Внимание, после того как на заказе появится хотя бы один нанятый исполнитель, " +
                        "изменить его параметры будет нельзя. Опубликовать заказ?"));
    }

    @Step("Подтвердить создание заказа и проверить результат")
    public void acceptOrderCreation() {
        $(new OrderListPage().onOrderCreationModalAlert().btnAccept).click();
        double deposit = Functions.apiGetDeposit(); // Получение depositAmount через API
        assertThat("Недостаточно средств для создания заказа", deposit, lessThan(Order.amount));
        $(new OrderListPage().onOrderListFrame().notification).shouldBe(visible)
                .shouldHave(text("Недостаточно свободных средств на депозите компании"));
        $(new OrderListPage().onOrderCreationPopup().popupContainer).shouldNotBe(visible);
        $$(new OrderListPage().onOrderListTable().orderStatus).get(0).shouldHave(text("Черновик"));
        assertThat("Созданный заказ есть в списке заказов",
                $$(new OrderListPage().onOrderListTable().orderTitle).get(0).getText(), matchesPattern(".*" + Order.name));
    }

    @Step("Подтвердить создание заказа и проверить параметры созданного заказа")
    public void acceptOrderCreationSuccessful() {
        $(new OrderListPage().onOrderCreationModalAlert().btnAccept).click();
        $(new OrderListPage().onOrderCreationPopup().popupContainer).shouldNotBe(visible);
        $(new OrderListPage().onOrderCreationSuccessPopup().textHeader).shouldHave(text("Заказ опубликован!"));
        $(new OrderListPage().onOrderCreationSuccessPopup().btnClose).click();
        assertThat("Созданный заказ есть в списке заказов",
                $$(new OrderListPage().onOrderListTable().orderTitle).get(0).getText(), matchesPattern(".*" + Order.name));
        $$(new OrderListPage().onOrderListTable().orderStatus).get(0).shouldHave(text("Поиск исполнителей"));
    }

    /**
     * Useful functions for different boring activities
     */
    static class Functions {

        static public void checkDoubleToggleCheckBox(ExBy element) {
            boolean oldStateChecked = $(element).isSelected();
            CommonFunctions.jsClick(element);
            if (oldStateChecked) {
                $(element).shouldBe(not(selected));
                CommonFunctions.jsClick(element);
                $(element).shouldBe(selected);
            } else {
                $(element).shouldBe(selected);
                CommonFunctions.jsClick(element);
                $(element).shouldBe(not(selected));
            }
        }

        static public double apiGetDeposit() {
            ApiAuthSteps.getTokensIfNeeds();
            try {
                GetInfoDepositBalanceModel response = Api.getInfoDepositBalance.getInfoDepositBalance("Bearer "
                        + Api.savedAccessToken).execute().body();
                assertThat("Ответ непустой", response, not(nullValue()));
                return response.depositAmount;
            } catch (IOException e) {
                assertThat("Запрос баланса успешно выполнен", e, nullValue());
                return 0;
            }
        }

        // 123234.1 --> "123 234 ₽"
        static public String formatAmount(Double amount) {
            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
            symbols.setGroupingSeparator(' ');
            DecimalFormat formatter = new DecimalFormat("###,### ₽", symbols);
            return formatter.format(amount);
        }
    }
}
