package steps;

import io.qameta.allure.Step;
import pageObject.pages.InfoPage;
import pageObject.pages.MainMenu;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class MainMenuSteps {

    @Step("Пеерход на страницу \"Заказы\"")
    public void openOrderListPage() {
        $(new MainMenu().onMainMenu().linkOrderList).click();
    }

    @Step("Проверка имени пользователя")
    public void checkUserName(String first, String last, String patronymic) {
        $(new InfoPage().onMainMenuFrame().divFirstName).shouldHave(text(first));
        $(new InfoPage().onMainMenuFrame().divSecondName).shouldHave(text(last));
        $(new InfoPage().onMainMenuFrame().divPatronymic).shouldHave(text(patronymic));
    }

}
