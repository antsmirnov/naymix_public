package api.service;

import api.model.AuthLoginModel;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface AuthLogin {
    @POST("auth/login/")
    @Headers("Content-Type: application/json")
    Call<AuthLoginModel> login(@Body RequestBody credentials);
}
