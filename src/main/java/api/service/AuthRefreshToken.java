package api.service;

import api.model.AuthLoginModel;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface AuthRefreshToken {
    @POST("auth/refreshToken/")
    @Headers("Content-Type: application/json")
    Call<AuthLoginModel> refreshToken(@Header("Authorization") String bearerAuthString);
}
