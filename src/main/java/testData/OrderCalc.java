package testData;

public class OrderCalc {
    public SalaryType salaryType;
    public Double amount;
    public Double commission;

    public OrderCalc(SalaryType salaryType, Double amount, Double commission) {
        this.salaryType = salaryType;
        this.amount = amount;
        this.commission = commission;
    }

    public Double getDepositValue() {
        if (salaryType == SalaryType.NET) {
            if (amount < 3000) {
                return amount * (100 + (6 + 4 + commission) / (100 - 6 - 4) * 100) / 100.0 + 40;
            } else {
                return amount * (100 + (6 + 4 + commission) / (100 - 6 - 4) * 100) / 100.0;
            }
        } else if (salaryType == SalaryType.GROSS) {
            if (amount < 3000) {
                return amount * (100 + commission) / 100.0 + 40;
            } else {
                return amount * (100 + commission) / 100.0;
            }
        } else {  // GROSS_WITHOUT_COMMISSION
            if (amount < 3000) {
                return amount * (100 - 6) * (100 + commission) / (100 - 10) / 100.0 + 40;
            } else {
                return amount * (100 - 6) * (100 + commission) / (100 - 10) / 100.0;
            }
        }
    }

    public Double getOrderValue() {
        if (salaryType == SalaryType.NET) {
            return amount * (100 + ((6 + 4 + commission) / (100 - 6 - 4) * 100)) / (100 + commission);
        } else if (salaryType == SalaryType.GROSS) {
            return amount;
        } else {  // GROSS_WITHOUT_COMMISSION
            return amount * (100 + ((4 + commission) / (100 - 4) * 100)) / (100 + commission);
        }
    }

    public Double getExecutorValue() {
        if (salaryType == SalaryType.NET) {
            return amount;
        } else if (salaryType == SalaryType.GROSS) {
            return amount * (100 - 6 - 4) / 100.0;
        } else {  // GROSS_WITHOUT_COMMISSION
            return amount * (100 - 6 - 4) / 100.0;
        }
    }

    public Double getTaxValue() {
        return getOrderValue() * 0.06;
    }

}