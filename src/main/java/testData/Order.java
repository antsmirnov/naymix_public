package testData;

public class Order {
    public static String category = "Реставрация";
    public static String subCategory = "Реставрация объектов архитектуры";
    public static String name = "Нужна реставрация здания";
    public static String orderDescription = "Историческое здание XII века";
    public static Double  amount = 200_000.0;
    public static Integer contractorsCount = 5;
    public static Double amountPerOneContractor = amount / contractorsCount;
}



