package testData;

public enum SalaryType {
    NET,
    GROSS,
    GROSS_WITHOUT_COMMISSION
}
