package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import service.Init;
import testData.OrderCalc;
import testData.SalaryType;
import testData.User;

public class OrderCalcTests extends Init {

    @BeforeClass
    public void openCalc() {
        baseSteps.login(User.login, User.password);
        mainMenuSteps.openOrderListPage();
        orderListCalcSteps.openCalc();
    }

    @DataProvider
    public Object[][] orderCalcCasesData() {
        return new Object[][]{
                {SalaryType.NET, 0.0, 0.0},
                {SalaryType.NET, 0.0, 100.0},
                {SalaryType.NET, 2000.0, 1.5},
                {SalaryType.NET, 2999.0, 0.0},
                {SalaryType.NET, 3000.0, 0.0},
                {SalaryType.NET, 3000.0, 1.5},
                {SalaryType.NET, 3001.0, 0.0},
                {SalaryType.NET, 3001.0, 1.5},
                {SalaryType.NET, 1_000_000.0, 0.0},
                {SalaryType.NET, 1_000_000.0, 100.0},

                {SalaryType.GROSS, 0.0, 0.0},
                {SalaryType.GROSS, 0.0, 100.0},
                {SalaryType.GROSS, 2000.0, 1.5},
                {SalaryType.GROSS, 2999.0, 0.0},
                {SalaryType.GROSS, 3000.0, 0.0},
                {SalaryType.GROSS, 3000.0, 1.5},
                {SalaryType.GROSS, 3001.0, 0.0},
                {SalaryType.GROSS, 3001.0, 1.5},
                {SalaryType.GROSS, 1_000_000.0, 0.0},
                {SalaryType.GROSS, 1_000_000.0, 100.0},

                {SalaryType.GROSS_WITHOUT_COMMISSION, 0.0, 0.0},
                {SalaryType.GROSS_WITHOUT_COMMISSION, 0.0, 100.0},
                {SalaryType.GROSS_WITHOUT_COMMISSION, 2000.0, 1.5},
                {SalaryType.GROSS_WITHOUT_COMMISSION, 2999.0, 0.0},
                {SalaryType.GROSS_WITHOUT_COMMISSION, 3000.0, 0.0},
                {SalaryType.GROSS_WITHOUT_COMMISSION, 3000.0, 1.5},
                {SalaryType.GROSS_WITHOUT_COMMISSION, 3001.0, 0.0},
                {SalaryType.GROSS_WITHOUT_COMMISSION, 3001.0, 1.5},
                {SalaryType.GROSS_WITHOUT_COMMISSION, 1_000_000.0, 0.0},
                {SalaryType.GROSS_WITHOUT_COMMISSION, 1_000_000.0, 100.0}
        };
    }

    @Test(dataProvider = "orderCalcCasesData")
    public void OrderCalcTests(SalaryType salaryType, Double amount, double commission) {
        orderListCalcSteps.setSalaryType(salaryType);
        orderListCalcSteps.setAmountAndCommission(amount, commission);
        orderListCalcSteps.checkDepositValue(new OrderCalc(salaryType, amount, commission));
        orderListCalcSteps.checkOrderValue(new OrderCalc(salaryType, amount, commission));
        orderListCalcSteps.checkExecutorValue(new OrderCalc(salaryType, amount, commission));
        orderListCalcSteps.checkTaxValue(new OrderCalc(salaryType, amount, commission));
    }
}
