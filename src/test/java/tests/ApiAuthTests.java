package tests;

import org.testng.annotations.Test;
import service.Init;

public class ApiAuthTests extends Init {

    @Test(description = "Проверка получения рабочего AccessToken-а", groups = "api")
    public void gettingGoodAccessTokens() {
        apiAuthSteps.getAndSaveTokens();
        apiDepositSteps.depositBalanceIsAccessible();
    }

    @Test(description = "Проверка получения AccessToken-а методом refreshToken",
            dependsOnMethods = "gettingGoodAccessTokens",
            groups = "api")
    public void gettingGoodAccessTokensViaRefreshToken() {
        apiAuthSteps.getAndSaveTokensWithRefreshToken();
        apiDepositSteps.depositBalanceIsAccessible();
    }
}
