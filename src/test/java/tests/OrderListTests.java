package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.Init;
import testData.User;

public class OrderListTests extends Init {
    @BeforeClass
    public void login() {
        baseSteps.login(User.login, User.password);
        mainMenuSteps.openOrderListPage();
    }

    @Test(description = "NAMEMIX-T741", groups = "UI")
    public void orderCreationTest() {
        orderListSteps.openNewOrderPopup();
        orderListSteps.firstPageFilling();
        orderListSteps.newOrderClickNextButton();
        orderListSteps.checkCheckBoxesCheckInOut();
        orderListSteps.checkCheckBoxesRequirePhoto();
        orderListSteps.newOrderClickNextButton();
        orderListSteps.checkPublicationWindow();
        orderListSteps.pubicTheOrder();
        orderListSteps.acceptOrderCreationSuccessful();
    }
}
