package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.Init;

public class ApiContractorStatusTests extends Init {

    @BeforeClass
    public void saveTokens() {
        apiAuthSteps.getAndSaveTokens();
    }

    @Test(description = "checkContractorStatus - проверка ответа при отправке существующего телефона и ИНН", groups = "api")
    public void checkContractorStatusWithValidInnAndPhone() {
        apiContractorStatusSteps.checkContractorStatusWithValidInnAndPhone();
    }
}
