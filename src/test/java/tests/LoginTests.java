package tests;

import org.testng.annotations.Test;
import service.Init;
import testData.User;

public class LoginTests extends Init {
    @Test(description = "Логин", groups = "UI")
    public void login() {
        baseSteps.login(User.login, User.password);
        mainMenuSteps.checkUserName(User.firstName, User.secondName, User.patronymic);
    }

    @Test(dependsOnMethods = "login", description = "Логаут", groups = "UI")
    public void logout() {
        baseSteps.logout();
        loginSteps.checkLoginPageTitle();
    }
}


